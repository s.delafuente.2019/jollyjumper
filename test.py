import unittest
import jolly

class JollyJumperTest(unittest.TestCase):
    def test_jolly(self):
        seq1 = [4 1 4 2 3]
        self.assertEqual(“jolly_jumper”, jolly.main(seq1))

if __name__ == "__main__":
    unittest.main()
